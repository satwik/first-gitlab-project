# Step by Step workflow of GitLab(Developer Version)
- Go to the GitLab Dashboard and search for the project which you want to work on.
![image_1](Images/1.png)
- Copy URL to clipboard (HTTPS)
![image_2](Images/2.png)

Click Copy URL to clipboard, your URL will be copied.
- Open Git Bash tool
- Navigate the present working directory to where you want your project to copy.
- Execute command below command:

   **$ git clone (URL)**

(The above command is to clone the git repository URL from git server into local machine)
![image_3](Images/3.png)

**Note:** Wanted to clone specific branch from remote repository to local machine. Use the command
  **$ git clone (URL) -b (branch name)** 
  - Navigate to the folder that has been cloned to your local machine using below command
  
  **$ cd (folder-name)**
  
  ![image_4](Images/4.png)
  - Check the status of the cloned repository by using below command

   **$ git status** 
 - (This command is used to see status about working directory and staging/index areas)
 ![image_5](Images/5.png)
 
 - Create a branch and checkout simultaneously by using below command
 
    **$ git checkout –b (branch name)**
 
![image_6](Images/6.png)

- To view the files and documents in the cloned repository, use below command:

   **$ ls –la or ll**

  ![image_7](Images/7.png)
  
- Do necessary changes that required for the folders/files and save them.
- When done with the changes, check the status of the repository using command 

   **$ git status**
   
![image_8](Images/8.png)

 - Now add those files to the staging area using below command
 
    **$ git add .**

**Note:**  Above command will add all the files and folders to the staging area. If you want to add specific file use the command **$ git add (file name)**
  
  ![images_10](Images/10.png)
  
  - Now commit those changes using below command

   **$ git commit –m “Commit message”**
   
![image_11](Images/11.png)

- Now check the status of the repository. **$ git status**

![image_12](Images/12.png)

**Note:** Till now changes have been made to the local repository and these changes have to be pushed to the GitLab (Remote Repository).

- Now push these changes to the remote repository using the command below command.

**$ git push –u origin (branch name)**

The above command is used to push files from local machine to remote repository (GitLab). Here “test” is a branch name. You can specify any branch. Before applying the branch name, check whether it is available or not. If it is not available, you should not apply that branch.

![image_13](Images/13.png)

**Note:** If prompted for GitLab Username and Password, enter your credentials. (It will be your LID and password)

- Now a new branch was created in GitLab repository which was ready to be merged into master branch. For that we have to create a merge request. Below are the steps to create merge request in GitLab.
- Go to your GitLab Dashboard and locate your project. Checkout to the branch that you have created. You should see like below image:

![image_14](Images/14.png)

- You can see new branch was successfully created by clicking the dropdown. Click on test branch, it will checkout to that particular branch.
- Go to the project where you'd like to merge your changes and click on the Merge requests which is located in sidebar as shown below.

![image_15](Images/15.png)

- After clicking the Merge Requests, you will be navigated to the below screen where you have to create a new merge requests.

![image_16](Images/16.png)

- Click on New merge request button, you will be navigated to below screen where you have the option to select the source branch and the target branch you'd like to compare to.

![image_17](Images/17.png)

- After selecting the source branch which you wanted to merge in the master branch, click on Compare branches and continue.
- At a minimum, add a title and a description to your merge request. Optionally, select a user to review your merge request and to accept or close it. You may also select a milestone and labels. 
- When ready, click on the Submit merge request button.
- Your merge request will be ready to be approved and merged. If this merge request is added to any issue, that issue will automatically closed.







  
 
 
 


  


 

  
