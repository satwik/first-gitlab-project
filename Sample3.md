# Wiki-GitLab
## Introduction
A separate system for documentation called Wiki, is built right into each GitLab project. It is enabled by default on all new projects and you can find it under Wiki in your project. Wikis are very convenient if you don't want to keep your documentation in your repository, but you do want to keep it in the same project where your code resides. You can create Wiki pages in the web interface or locally using Git since every Wiki is a separate Git repository.

## First time creating the Home page
* Navigate to GitLab dashboard and search project for which you want to create Wiki.

![image_1](Images/Wiki Images/1.png)

* After selecting the specific project, you should able to see similar to the below screen.

![image_2](Images/Wiki Images/2.png)

* Click on the Wiki which is located in side bar after selecting your project.

![image_3](Images/Wiki Images/3.png)

* The first time you visit a Wiki, you will be directed to create the Home page. The Home page is necessary to be created since it serves as the landing page when viewing a Wiki. You should see below screen:

![image_4](Images/Wiki Images/4.png)

* You only have to fill in the Content section and click Create page. You can always edit it later, so go ahead and write a welcome message. Home page will be created.

## Creating a new wiki page

* Create a new page by clicking the New page button that can be found in all wiki pages. You will be asked to fill in the page name from which GitLab will create the path to the page. 

![image_5](Images/Wiki Images/5.png)

* After clicking the New page, you will be navigated to below screen where you have to provide the Page slug.

![image_6](Images/Wiki Images/6.png)

* After entering the Page slug, click on Create page. A new wiki page will be created in your project. You can specify a full path for the new file and any missing directories will be created automatically.
- After entering the page name, it's time to fill in its content. GitLab wikis supports three formats.
 1. Markdown
 1. RDoc
 1. AsciiDoc

**Note:**  For Markdown based pages, all the Markdown features are supported and for links there is some wiki specific behavior.

![image_7](Images/Wiki Images/7.png)

**Note:**  In the web interface the commit message is optional, but the GitLab Wiki is based on Git and needs a commit message, so one will be created for you if you do not enter one.

## Editing Wiki Page
- To edit a page, simply click on the Edit button which is located in upper right corner of the wiki dashboard which is shown in below image.

![image_8](Images/Wiki Images/8.png)

- When you click on Edit button, you will be navigated to below screen where you can change wiki content. When done, Click Save changes for the changes to take effect.

![image_9](Images/Wiki Images/9.png)

## Deleting a Wiki page

- You can find the Delete button only when editing a page.

![image_10](Images/Wiki Images/10.png)

- Click on it and confirm you want the page to be deleted from the project.

## History of Wiki Page

- The changes made to wiki page over time are recorded in the wiki's Git repository, and you can view them by clicking the Page history button.

![image_11](Images/Wiki Images/11.png)

- When you click on Page history button, you will be navigated to below screen where you can see the Page Version, Author, Commit message, Last updated and Format of the wiki page.

![image_12](Images/Wiki Images/12.png)

- To see the changes in previous version of the page, click on Page version number.

## Cloning wiki repository locally

- You can clone wiki page locally as they were Git based repositories. You can edit them like other Git repositories. To clone Wiki repository locally, click on the Clone Repository on the right side bar which is shown in below image.

![image_13](Images/Wiki Images/13.png)

- After clicking Clone repository button, you will be navigated to screen where you will have set of instructions to clone wiki repository. The page looks as below image.

![image_14](Images/Wiki Images/14.png)

- Click Copy URL to clipboard, your URL will be copied. Open Git Bash tool and navigate the present working directory to where you want your wiki repository to copy. Execute command below command:

**$ git clone (URL)** 

![image_15](Images/Wiki Images/15.png)

- Navigate to the folder that has been cloned to your local machine using below command:

**$ cd (folder-name)**
- To view the files and documents in the cloned wiki repository, use below command:

**$ ls –la or ll**

![image_16](Images/Wiki Images/16.png)

- Do necessary changes that required for the folders/files and save them. Now add those files to the staging area using command **$ git add .**

![image_17](Images/Wiki Images/17.png)

- Now commit those changes by using command **$ git commit –m “Commit Message”**

![image_18](Images/Wiki Images/18.png)

**Note:** Till now changes have been made to the local wiki repository and these changes have to be pushed to the GitLab (Remote Repository).

- Now push these changes to the remote repository using the command below command.

**$ git push –u origin master**

![image_19](Images/Wiki Images/19.png)

**Note:** If prompted for GitLab Username and Password, enter your credentials. (It will be your LID and password)
- Now you can see the changes has been reflected to the remote wiki repository which is shown in below image.

![image_20](Images/Wiki Images/20.png)

- These are the steps should follow when you want to clone the wiki repository locally and edit the wiki pages.






 




















 




