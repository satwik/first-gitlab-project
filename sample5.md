# Standard Asset Documentation
## Step by Step procedure to submit Standard Assets
- Navigate to GitLab tool using URL https://gitlabmedical.highmark.com and enter your LDAP Username and Password.

![image_1](Images/StandardAsset Screenshots/1.png)
- After signing in you will be navigated to GitLab Dash board where your projects will be displayed.
- Navigate to ``Standard-Assets`` project using search bar provided at Upper right corner or Use below URL which directly takes you the Standard-Assets Project.
     
     https://gitlabmedical.highmark.com/AAR-Asset/Standard-Assets
- After navigating to the Standard-Assets project, you should be able to see below screen.

![image_2](Images/StandardAsset Screenshots/2.png)
- Now create an issue. For that click on the **Issues** which is present in the Sidebar.

![image_3](Images/StandardAsset Screenshots/3.png)

- After Clicking on the Issues, you will be navigated to screen where you will have an option **New Issue**.

![image_4](Images/StandardAsset Screenshots/4.png)

- After clicking the **New issue** button, you will be navigated to Issue page where you will be filling the necessary information required to submit the issue. Screen should look like below image.

![image_5](Images/StandardAsset Screenshots/5.PNG)

- Choose the issue template from the drop down. At present, there are three issue templates.
    1. NewStandardData
    1. NewStandardLogic
    1. NewStandardTool

![image_6](Images/StandardAsset Screenshots/6.PNG)

- After choosing required template from drop down, standard template will be shown in Description Dialogue box. Fill in the information that required for Standard Asset in that template.

![image_7](Images/StandardAsset Screenshots/7.png)

- After filling the necessary information, click on **Submit issue**. Your issue will be submitted and ready to be viewed by the Group Members.
Note: For **“Data Location”** in template,**"standard assets"** will live in your original location of GitLab. But, have to link the URL here in **Markdown** format. 
- You can view the group members of the team by clicking **Members** which is located in side bar.

![image_8](Images/StandardAsset Screenshots/8.png)

- When you click on the Members, **“StandardResourceWorkGroup”** members list will be shown. The issue submitted will be reviewed by that group. You can track the issue movement in the **Board** which is shown in below image.

![image_9](Images/StandardAsset Screenshots/9.png)

- When you click on the **Board**, you will be navigated to issue tracking board where you can see the movement of issue under different sections.

![image_10](Images/StandardAsset Screenshots/10.png)

- To explain above image, when you’ve submitted the issue that will be added to ``Backlog``. Issue will be moved to ``Under Review`` list when the group members are reviewing the issue.

**Note:** Comments and Reviews are given to the submitted issue in this zone. If any changes need to be done to the submitted issue, you will be notified.

- If group members done with the review and got up votes to the Standard Asset, issue will be moved to ``StandardAsset`` list. Now issue will be tagged with **StandardAsset** label.
- These are the steps should be taken to submit the Standard Asset to GitLab.












