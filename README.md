# Sample Project

#  Create a Project
 1. In your dashboard, click the green **New project button** or use the **plus** icon in the upper right corner of the navigation bar.
    
    ![](https://docs.gitlab.com/ee/gitlab-basics/img/create_new_project_button.png)

 2. This opens the New project page.

 3. Choose if you want start a **blank project**, or with one of the predefined Project Templates: this will kickstart your repository code and CI automatically. Otherwise, if you have a project in a different repository, you can import it by clicking on the Import project tab, provided this is enabled in your GitLab instance. Ask your administrator if not. 

 4. Provide the following information:
- Enter the name of your project in the Project name field. You can't use special characters, but you can use spaces, hyphens, underscores or even emoji.
- The Project description (optional) field enables you to enter a description for your project's dashboard, which will help others understand what your project is about. Though it's not required, it's a good idea to fill this in.
- Changing the Visibility Level modifies the project's viewing and access rights for users

 5. Click **Create Project**.

# Create an Issue

 We can create issues in many ways. One of them are described below:
 
 When you create a new issue, you'll be prompted to fill in the information illustrated on the image below.

   ![](https://docs.gitlab.com/ee/user/project/issues/img/new_issue.png)

 Click on **Submit issue**. New Issue will be created.

# Lables
Labels allow you to categorize issues or merge requests using descriptive titles like bug, feature request, or docs. Each label also has a customizable color. 

## Creating Lables
  **Note:** A permission of **Developer** or higher is required to create lable.

## New project label 
- To create a project label, navigate to **Issues** > **Labels** in the project.

- Click the **New label** button. Enter the title, an optional description, and the background color. Click **Create label** to create the label.

- If a project has no labels, you can generate a default set of project labels from its empty label list page

  ![](https://docs.gitlab.com/ee/user/project/img/labels_generate_default.png)

## New group label 
- To create a group label, follow similar steps from above to project labels. Navigate to **Issues** > **Labels** in the group and create it from there. 
- Alternatively, you can create group labels also from Epic sidebar. Please note that the created label will belong to the immediate group to which epic belongs.

## Editing labels 
**Note:** A permission level of **Developer** or higher is required to edit labels.

You can update a label by navigating to **Issues** > **Labels** in the project or group and clicking the pencil icon.

You can delete a label by clicking the **trash icon**.  

# Filtering issues, merge requests and epics by label 
## Filtering in list pages 
- From the project issue list page and the project merge request list page, you can filter by both group (including subgroup ancestors) labels and project labels.

- From the group issue list page and the group merge request list page, you can filter by both group labels (including subgroup ancestors and subgroup descendants) and project labels.

- From the group epic list page, you can filter by both current group labels as well as decendent group labels.

# Milestones[](#milestones "Permalink")

## Overview[](#overview "Permalink")

Milestones in GitLab are a way to track issues and merge requests created to achieve a broader goal in a certain period of time.

Milestones allow you to organize issues and merge requests into a cohesive group, with an optional start date and an optional due date.

## Project milestones and group milestones 
- **Project milestones** can be assigned to issues or merge requests in that project only.
- **Group milestones** can be assigned to any issue or merge request of any project in that group.
- In the future, you will be able to assign group milestones to issues and merge requests of projects in subgroups.

## Creating milestones 
**Note:** A permission level of **Developer** or higher is required to create milestones.

## New project milestone 
To create a project milestone, navigate to **Issues** > **Milestones** in the project.

Click the **New milestone** button. Enter the title, an optional description, an optional start date, and an optional due date. Click **Create milestone** to create the milestone.

  ![](https://docs.gitlab.com/ee/user/project/milestones/img/milestones_new_project_milestone.png)

## New group milestone 
To create a group milestone, follow similar steps from above to project milestones. Navigate to **Issues** > **Milestones** in the group and create it from there.
  
  ![](https://docs.gitlab.com/ee/user/project/milestones/img/milestones_new_group_milestone.png)

## Editing milestones 
**Note:** A permission level of **Developer** or higher is required to edit milestones.

- You can update a milestone by navigating to **Issues** > **Milestones** in the project or group and clicking the **Edit** button.

- You can delete a milestone by clicking the **Delete** button.

## Project milestone features 
These features are only available for project milestones and not group milestones.

- Issues assigned to the milestone are displayed in three columns: Unstarted issues, ongoing issues, and completed issues.
- Merge requests assigned to the milestone are displayed in four columns: Work in progress merge requests, waiting for merge, rejected, and closed.
- Participants and labels that are used in issues and merge requests that have the milestone assigned are displayed.
- Burndown chart.

## Milestone sidebar 
The milestone sidebar on the milestone view shows the following:

- Percentage complete, which is calculated as number of closed issues plus number of closed/merged merge requests divided by total number issues and merge requests.
- The start date and due date.
- The total time spent on all issues that have the milestone assigned.
- For project milestones only, the milestone sidebar shows the total issue weight of all issues that have the milestone assigned.

  ![](https://docs.gitlab.com/ee/user/project/milestones/img/milestones_project_milestone_page.png)

# Create Merge Request and Close the Issue
## Merge requests
Merge requests allow you to exchange changes you made to source code and collaborate with other people on the same project.

## How to create a merge request
Merge requests are useful to integrate separate changes that you've made to a project, on different branches. This is a brief guide on how to create a merge request.
1. Before you start, you should have already created a branch and pushed your changes to GitLab.
1. Go to the project where you'd like to merge your changes and click on the Merge requests tab.
1. Click on New merge request on the right side of the screen.
1. From there on, you have the option to select the source branch and the target branch you'd like to compare to. The default target project is the upstream repository, but you can choose to compare across any of its forks.                                                                       
 ![](https://docs.gitlab.com/ee/gitlab-basics/img/merge_request_select_branch.png)
1. When ready, click on the Compare branches and continue button.
1. At a minimum, add a title and a description to your merge request. Optionally, select a user to review your merge request and to accept or close it. You may also select a milestone and labels.
1. When ready, click on the Submit merge request button.                                                                                           
        ![](https://docs.gitlab.com/ee/gitlab-basics/img/merge_request_page.png)

Your merge request will be ready to be approved and merged. If this merge request is added to any issue, that issue will automatically closed.


# Wiki
- A separate system for documentation called **Wiki**, is built right into each GitLab project. It is enabled by default on all new projects and you can find it under **Wiki** in your project.
- Wikis are very convenient if you don't want to keep your documentation in your repository, but you do want to keep it in the same project where your code resides.
- You can create Wiki pages in the web interface or locally using Git since every Wiki is a separate Git repository.
## First time creating the Home page 
The first time you visit a Wiki, you will be directed to create the **Home page**. The Home page is necessary to be created since it serves as the landing page when viewing a Wiki. You only have to fill in the Content section and click **Create page**. You can always edit it later, so go ahead and write a welcome message.
 
   ![](https://docs.gitlab.com/ee/user/project/wiki/img/wiki_create_home_page.png)

## Creating a new wiki page 
Create a new page by clicking the New page button that can be found in all wiki pages. You will be asked to fill in the page name from which GitLab will create the path to the page. You can specify a full path for the new file and any missing directories will be created automatically.
   
   ![](https://docs.gitlab.com/ee/user/project/wiki/img/wiki_create_new_page_modal.png)

Click the **Create page** and the new page will be created.

   ![](https://docs.gitlab.com/ee/user/project/wiki/img/wiki_create_new_page.png)

## Editing a wiki page 
To edit a page, simply click on the **Edit** button. From there on, you can change its content. When done, click **Save changes** for the changes to take effect.

## Deleting a wiki page 
You can find the **Delete** button only when editing a page. Click on it and **confirm** you want the page to be deleted.   

  



  
