# Step by Step Workflow of GitLab(Business User Version)
- Go to the GitLab Dashboard and search for the project which you want to work on.

![image_1](Images/Bimages/1.png)

- After selecting the specific project, you should able to see similar to the below screen.

![image_2](Images/Bimages/2.png)

- Next step is to create a branch. Click on ‘+’ symbol which is clearly shown in above image. A dropdown will appear, which looks like below image.

(Branch: A branch is an independent line of development. To add changes to your GitLab project, you should create a branch. You can do it in your terminal or by using the web interface. Below steps shows how to create a branch using web interface.)

![image_3](Images/Bimages/3.png)

- Click on New branch, this is will take to you the following screen where you need to enter the branch name which you wanted to create.

![image_4](Images/Bimages/4.png)

- Enter the Branch name and Click on Create from dropdown, you will see the list of branches available and select any one branch. (Default is master branch).

![image_5](Images/Bimages/5.png)

- Click on Create branch. New branch will be created from the branch you have selected from “Create from” dropdown
- After clicking on Create branch, you will be checkout automatically to the new branch which created. You should see the following screen where you will do your operations which are required.

![image_6](Images/Bimages/6.png)

**Note:** The newly created branch will not disturb any of the files/documents which are present in the remaining branches when you do changes in new branch.
- Here are the steps to Upload file to your branch from your local machine. After checkout to the branch which you wanted to work on, click on ‘+’ icon, you will see a dropdown where you select “Upload file”. File can be any format (Example: word, spreadsheet, data file, image etc.).
- Click on Upload file. You should see below screen, where you can attach a file by drag & drop or click to upload. After uploading document, write the commit message and include the target branch.

![image_7](Images/Bimages/7.png)

- After filling necessary information, click on “Upload file”. Your file will be successfully uploaded to the remote repository (GitLab) from your local machine. 
- If you wanted to merge all your branch changes into master branch, you have to create a merge request. Below are the sequence of steps to create a merge request.

(Merge Request: Merge requests allow you to exchange changes you made to source code and collaborate with other people on the same project. A Merge Request (MR) is the basis of GitLab as a code collaboration and version control platform. Is it simple as the name implies: a request to merge one branch to another.)

- Go to the project where you'd like to merge your changes and click on the Merge request which is located in sidebar as shown below

![image_8](Images/Bimages/8.png)

- After clicking the Merge Requests, you will be navigated to the below screen where you have to create a new merge requests.

![image_9](Images/Bimages/9.png)

- Click on New merge request button, you will be navigated to below screen where you have the option to select the source branch and the target branch you'd like to compare to.

![image_10](Images/Bimages/10.png)

- After selecting the source branch which you wanted to merge in the master branch, click on Compare branches and continue.
- At a minimum, add a title and a description to your merge request. Optionally, select a user to review your merge request and to accept or close it. You may also select a milestone and labels. 
- When ready, click on the Submit merge request button.
- Your merge request will be ready to be approved and merged. If this merge request is added to any issue, that issue will automatically closed.

**Note:** If project members/owner have made some suggestions to do changes in the document/file, you have to upload document/file once again by doing necessary changes and commit those changes.







