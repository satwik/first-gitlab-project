# Steps to Create a Project in GitLab
- Navigate to GitLab dashboard. Click the green **“New project”** or use the **plus** icon in the upper right corner of the navigation bar. From the drop down choose **New project**.

![image_1](Images/CreateProject Screenshots/1.png)

- Click either of the options which takes to the same screen, this open the **New Project** page which looks like below image.

![image_2](Images/CreateProject Screenshots/2.png)

**Note:** A project is where you house your files (**repository**), plan your work (**issues**), and publish your documentation (**Wiki**) among other things. All new features are enabled when you create a project, but you can disable the ones you don’t need in the **project settings**.
- New project can be created in three different ways in GitLab.
    - Blank project
    - Create from template
    - Import project

### Below  steps will guide to create a Blank project:
1. Click on **Blank project** tab.
2. Enter the name of your project in the **Project name** field. You can't use special characters, but you can use spaces, hyphens, underscores or even emoji.
3. Enter **Project description (optional)** in the field provided which enables you to enter a description for your project. *(Adding Project description will help others/team members understand what exactly your project is about. Even though it is an optional, it’s a good practice to include.)*
4. Select the **Visibility Level**, which modifies the project’s viewing and access rights for users. There are **three** Visibility Levels.
    - **Private:** Project access must be granted explicitly to each other
    -  **Internal:** The project can be accessed by any logged in user
    -  **Public:** The project can be accessed without any authentication
5. After filling the necessary information, click on **Create project** button.

![image_3](Images/CreateProject Screenshots/3.png)

### Below steps will guide to create project from template:
  1. Click on Create from template tab. You will be navigated to below screen.

![image_4](Images/CreateProject Screenshots/4.png)
    
   2. Use any one of the template, you can also preview the template by hitting **“Preview”** and Click on Use template.  You will be navigated to below screen. (Selected **“Ruby on Rails”** as sample template). If got any predefined templates, you can use that as well.

![image_5](Images/CreateProject Screenshots/5.png)

   3. Enter necessary information on the fields provided and choose **Visibility Level** and Click on **Create project**. A new project will be create with selected template.

- If you have a project in a different repository, you can import it by clicking on the Import project tab, provided this is enabled in your GitLab instance. Contact your GitLab Administrator if or not.




